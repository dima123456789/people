<?php

class DeferredPersonDataJobTest extends \Codeception\Test\Unit
{
    /**
     * @var $tester \UnitTester
     */
    public $tester;
    
    public function testSavePersonData()
    {
        $personData = new \app\models\PersonData();
        
        $personData->phoneNumbers = [
            '099 999-99-99',
            '066 666-66-66',
        ];
        
        $personData->firstName = 'John';
        $personData->lastName = 'Doe';
    
        /** @var \app\jobs\DeferredPersonDataJob $job */
        $job = $this->getMockBuilder(\app\jobs\DeferredPersonDataJob::class)
                    ->setConstructorArgs(['personData' => $personData])
                    ->getMock();
        
        $job->savePersonData();
    
        /** @var \app\models\Person $person */
        $person = $this->tester->grabRecord(\app\models\Person::class, ['first_name' => 'John']);
        
        expect($person)->isInstanceOf(\app\models\Person::class);
        
        expect($person->first_name)->equals('John');
        expect($person->last_name)->equals('Doe');
    
        /** @var \app\models\Phone $phone */
        $phone = $this->tester->grabRecord(\app\models\Phone::class, ['number' => '099 999-99-99']);
        
        expect($phone)->isInstanceOf(\app\models\Phone::class);
        
        expect($phone->person_id)->equals($person->person_id);
    }
}