<?php 

class PersonCest
{
    public function _before(FunctionalTester $I)
    {
    }
    
    public function personAddTest(FunctionalTester $I)
    {
        $I->haveHttpHeader('Content-Type', 'application/json');
    
        $payload = '{
    "firstName": "Dima",
    "lastName": "Mytianskyi",
    "phoneNumbers": [
        "098 588-78-77",
        "066 257-70-08"
    ]
}';
        $I->sendPUT('/person', $payload);
        
        $I->seeResponseContains('success');
    }
    
    public function personFindWithoutQueryTest(FunctionalTester $I)
    {
        $I->haveRecord(\app\models\Person::class, ['first_name' => 'John', 'last_name' => 'Doe', 'person_id' => 2]);
        
        $I->haveRecord(\app\models\Phone::class, ['person_id' => 2, 'number' => '099 999-99-99']);
        
        
        $I->sendGET('/person');
        
        $I->seeResponseIsJson();
        
        $I->seeResponseEquals(\yii\helpers\Json::encode(
            [
                (object)[
                    'first_name' => 'John',
                    'last_name' => 'Doe',
                    'number' => '099 999-99-99'
                ]
            ]
        ));
    }
}
