<?php
$params = require __DIR__ . '/params.php';
$db = require __DIR__ . '/db.php';
$rabbit = require __DIR__ . '/rabbit.php';

/**
 * Application configuration shared by all test types
 */
return [
    'id' => 'basic-tests',
    'basePath' => dirname(__DIR__),
    'aliases' => [
        '@bower' => '@vendor/bower-asset',
        '@npm'   => '@vendor/npm-asset',
    ],
    'language' => 'en-US',
    'components' => [
        'db' => $db,
        'request' => [
            // !!! insert a secret key in the following (if it is empty) - this is required by cookie validation
            'cookieValidationKey' => 'VngTqWKOacEJz0VRvjQFV1_HYrm6YSjC',
        ],
        'response' => [
            'class' => \yii\web\Response::class,
            'format' => \yii\web\Response::FORMAT_JSON,
        ],
        'urlManager' => [
            'enablePrettyUrl' => true,
            'showScriptName' => false,
            'enableStrictParsing' => true,
            'rules' => [
                [
                    'route' => 'site/person-get',
                    'pattern' => '/person/<q:(\w+)?>',
                    'verb' => ['GET'],
                    'defaults' => [
                        'q' => '',
                    ]
                ],
                [
                    'route' => 'site/person-put',
                    'pattern' => '/person',
                    'verb' => ['put'],
                ],
                [
                    'route' => 'site/search',
                    'pattern' => '/search',
                    'verb' => ['get'],
                ],
            ],
        ],
        'user' => [
            'identityClass' => 'app\models\User',
        ],
        'rabbit' => $rabbit,
    ],
    'params' => $params,
];
