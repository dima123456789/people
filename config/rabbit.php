<?php
return [
    'class' => \yii\queue\amqp_interop\Queue::class,
    'port' => 5672,
    'host' => 'rabbit',
    'vhost' => 'rabbit_vhost',
    'user' => 'rabbit_user',
    'password' => 'rabbit_pass',
    'driver' => \yii\queue\amqp_interop\Queue::ENQUEUE_AMQP_LIB,
];