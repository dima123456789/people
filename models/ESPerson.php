<?php

namespace app\models;

use yii\elasticsearch\ActiveRecord;

/**
 * @property string first_name
 * @property string last_name
 * @property array number
 */
class ESPerson extends ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function index()
    {
        return 'people';
    }
    
    /**
     * @inheritdoc
     */
    public static function type()
    {
        return 'person';
    }
    
    public static function mapping()
    {
        return [
            static::type() => [
                'properties' => [
                    'first_name' => ['type' => 'string'],
                    'last_name' => ['type' => 'string'],
                    'number' => ['type' => 'string'],
                ]
            ]
        ];
    }
    
    public function attributes()
    {
        return [
            'first_name',
            'last_name',
            'number',
        ];
    }
}