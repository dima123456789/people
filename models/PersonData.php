<?php

namespace app\models;

use yii\base\Model;

class PersonData extends Model implements IPersonData
{
    public $firstName;
    
    public $lastName;
    
    public $phoneNumbers = [];
    
    public function rules()
    {
        return [
            [['firstName', 'lastName'], 'required'],
            [['firstName', 'lastName'], 'string'],
            ['phoneNumbers', 'safe'],
        ];
    }
    
    public function getFirstName() : string
    {
        return $this->firstName;
    }
    
    public function getLastName() : string
    {
        return $this->lastName;
    }
    
    public function getNumbers() : array
    {
        return $this->phoneNumbers;
    }
}