<?php

namespace app\models;

interface IPersonData
{
    public function getFirstName() : string;
    
    public function getLastName() : string;
    
    public function getNumbers() : array;
}