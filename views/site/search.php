<?php

/* @var $this yii\web\View */

$this->title = '';
\yii\web\AssetBundle::register($this);
?>
<div class="site-index">
    <div data-row-template="<?= $this->render('_rowTemplate') ?>">
    
    </div>
    <div class="jumbotron">
        <?= \yii\helpers\Html::input('input', 'person_search', '', ['id' => 'person_search_input']) ?>
        <?= \yii\helpers\Html::button('Find', ['id' => 'person_search_button']) ?>

        <div class="list"></div>
    </div>
</div>
