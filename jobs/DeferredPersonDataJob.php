<?php

namespace app\jobs;

use app\models\ESPerson;
use app\models\IPersonData;
use app\models\Person;
use app\models\Phone;

//TODO: config docker to run queue by default

class DeferredPersonDataJob extends \yii\base\BaseObject implements \yii\queue\JobInterface
{
    public $logCategory = 'rabbit_job';
    
    private $personData = null;
    
    public function __construct(IPersonData $personData, $config = [])
    {
        parent::__construct();
        
        $this->personData = $personData;
    }
    
    /**
     * @param \yii\queue\Queue $queue which pushed and is handling the job
     *
     * @return void|mixed result of the job execution
     */
    public function execute($queue)
    {
        try {
            $this->savePersonData();
            $this->esSavePersonData();
        } catch (\Exception $e) {
            \Yii::error($e->getMessage(), $this->logCategory);
        }
    }
    
    private function savePersonData()
    {
        $person = new Person;
    
        $person->first_name = $this->personData->getFirstName();
        $person->last_name = $this->personData->getLastName();
    
        if (!$person->save()) {
            \Yii::error(sprintf('Person validation failed %s', serialize($person->getErrors())), $this->logCategory);
        
            return;
        }

        foreach ($this->personData->getNumbers() as $number) {
            $phone = new Phone;
            $phone->number = $number;
        
            if ($phone->validate('number')) {
                $person->link('phones', $phone);
            } else {
                \Yii::error(
                    sprintf(
                        'Phone validation for person_id %d failed. %s',
                        $person->person_id,
                        $phone->getErrors()
                    ),
                    $this->logCategory
                );
            }
        }
    }
    
    private function esSavePersonData()
    {
        foreach ($this->personData->getNumbers() as $number) {
            $esPerson = new ESPerson;
            $esPerson->first_name = $this->personData->getFirstName();
            $esPerson->last_name = $this->personData->getLastName();
            $esPerson->number = $number;
            \Yii::info(serialize($esPerson), $this->logCategory);
    
            if (!$esPerson->save()) {
                \Yii::info(serialize($esPerson->getErrors()), $this->logCategory);
            } else {
                \Yii::info('saved', $this->logCategory);
            }
        }
    }
}