#!/usr/bin/env bash

if [ ! -d "vendor" ]; then
    composer install
fi

#php yii rabbit/listen
php-fpm -F -e