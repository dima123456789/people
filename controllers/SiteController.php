<?php

namespace app\controllers;

use app\factories\InvalidPersonDataException;
use app\factories\PersonDataFactory;
use app\jobs\DeferredPersonDataJob;
use app\models\IPersonData;
use app\models\Person;
use InvalidArgumentException;
use yii\web\BadRequestHttpException;
use yii\web\Controller;
use yii\helpers\Json;
use yii\web\Response;

class SiteController extends Controller
{
    public $enableCsrfValidation = false;
    
    public function actionPersonGet($q)
    {
//        return ESPerson::find()->query(['match' => ['first_name' => $q'])->all();
        return Person::find()
                     ->select(['person.first_name', 'person.last_name', 'phone.number'])
                     ->joinWith('phones', false)
                     ->andFilterWhere(['like', 'first_name', $q])
                     ->asArray()
                     ->all();
    }
    
    public function actionPersonPut()
    {
        $json = file_get_contents('php://input');
    
        try {
            $payload = Json::decode($json);
        
            /** @var IPersonData $personData */
            $personData = PersonDataFactory::makePersonData($payload);
        } catch (InvalidArgumentException $e) {
            throw new BadRequestHttpException(sprintf('Invalid payload json format. %s', $e->getMessage()));
        } catch (InvalidPersonDataException $e) {
            throw new BadRequestHttpException(sprintf('Error making person data object. %s', $e->getMessage()));
        }
    
        $job = new DeferredPersonDataJob($personData);
    
        \Yii::$app->rabbit->push($job);
    
        return 'success';
    }
    
    public function actionSearch()
    {
        \Yii::$app->response->format = Response::FORMAT_HTML;
        
        return $this->render('search');
    }
}
