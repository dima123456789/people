$(function () {
  updateList();

  $('#person_search_button').click(function () {
    var q = $('#person_search_input').val();
    updateList(q);
  })
});

function updateList(q)
{
  $('.list').empty();

  var url = '/person';
  if (typeof q !== 'undefined') {
    url += '/' + q;
  }

  $.ajax({
    url: url,
    complete: function (data) {
      var persons = data.responseJSON;
      for (var i in persons) {
        render(persons[i]);
      }
    }
  })
}

function render(person)
{
  var template = $('[data-row-template]').data('rowTemplate');

  var $container = $('.list');
  var row;

  row = template;
  for (var key in person) {
    row = row.replace('{{' + key + '}}', person[key]);
  }

  $container.append(row);
}