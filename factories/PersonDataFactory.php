<?php

namespace app\factories;

use app\models\IPersonData;
use app\models\PersonData;
use yii\base\InvalidArgumentException;

class PersonDataFactory
{
    public static function makePersonData(array $params) : IPersonData
    {
        $personData = new PersonData;
    
        if (!$personData->load($params, '')) {
            throw new InvalidArgumentException('No data');
        }
    
        if (!$personData->validate()) {
            throw new InvalidArgumentException(
                sprintf('Input data has not been validated: %s', serialize($personData->getErrors()))
            );
        }
        
        return $personData;
    }
}