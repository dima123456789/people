<?php

namespace app\factories;

use yii\base\InvalidArgumentException;

class InvalidPersonDataException extends InvalidArgumentException
{
    public function getName()
    {
        return 'Invalid person data';
    }
}