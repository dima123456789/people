<?php

use yii\db\Migration;

/**
 * Class m180925_164146_init_person_table
 */
class m180925_164146_init_person_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable(
            'person',
            [
                'person_id' => 'INT UNSIGNED AUTO_INCREMENT PRIMARY KEY',
                'first_name' => 'VARCHAR(255) NOT NULL DEFAULT \'\'',
                'last_name' => 'VARCHAR(255) NOT NULL DEFAULT \'\'',
            ],
            'CHARSET=utf8'
        );
        
        $this->createIndex('uk_person_first_name_last_name', 'person', ['first_name', 'last_name'], true);
        
        $this->createTable(
            'phone',
            [
                'phone_id' => 'INT UNSIGNED AUTO_INCREMENT PRIMARY KEY',
                'person_id' => 'INT UNSIGNED NOT NULL',
                'number' => 'CHAR(13) NOT NULL',
            ]
        );
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('phone');
        $this->dropTable('person');
        
        return true;
    }
}
