<?php

use app\models\ESPerson;
use yii\db\Migration;

/**
 * Class m180928_132135_init_elastic
 */
class m180928_132135_init_elastic extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        ESPerson::getDb()->createCommand()->createIndex(ESPerson::index(), ['mappings' => ESPerson::mapping()]);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        ESPerson::getDb()->createCommand()->deleteIndex(ESPerson::index());
        
        return true;
    }
}
